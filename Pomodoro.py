import subprocess as sp
import time
import os
import pygame

pygame.init()


class Pomodoro:
    def __init__(self, n, Time, short_break, long_break):
        self.pomodoros = n
        self.time = Time
        self.short_break = short_break
        self.long_break = long_break

    def pomodoro_start(self, n):
        Time = self.time * 60
        self.pomo_status(n, Time, 'Pomodoring...')

        Notify.end(self, "Pomodoro finished")

    def short_break_start(self, n):
        Time = self.short_break * 60
        self.pomo_status(n, Time, 'Enjoy your short break!')

        Notify.end(self, "Short break finished")

    def long_break_start(self, n):
        Time = self.long_break * 60
        self.pomo_status(n, Time, 'Enjoy your long break!')

        Notify.end(self, "Long break finished")

    @staticmethod
    def text(n):
        current_time = time.strftime('%l:%M %p')
        text = f'{current_time} \tPomodoro {n} -'
        return text

    def pomo_status(self, n, Time, pomo):
        while Time >= 0:
            if Time == 10:
                Notify.timer(self)

            status = time.strftime('%M:%S', time.gmtime(Time))
            print(f'{Pomodoro.text(n)} {pomo} ({status})', end='\r')
            Time -= 1
            time.sleep(1)


class Notify:
    def timer(self):
        pygame.mixer.music.load('files/clock_timer.wav')
        pygame.mixer.music.play()

    def end(self, text):
        img = os.getcwd()+'/files/tomato.png'
        sp.Popen(['notify-send', '-i', img, 'Pomodoro', text])
        pygame.mixer.music.load('files/sound.wav')
        pygame.mixer.music.play()


class Menu(Pomodoro):
    def __init__(self):
        Pomodoro.__init__(self, n, Time, short_break, long_break)
        self.start()

    def start(self):
        for n in range(self.pomodoros):
            # Pomodoro
            ans = str(input(f'{Pomodoro.text(n+1)} Do you want to start '
                            f'pomodoro ({self.short_break:.0f} min)? (y/n) '))
            ans = ans.strip().lower()
            os.system('clear')

            if ans == 'y':
                self.pomodoro_start(n+1)

            # Short Break
            ans = str(input(f'{Pomodoro.text(n+1)} Do you want to start '
                            f'short break ({self.short_break:.0f} min)? (y/n) '))
            ans = ans.strip().lower()
            os.system('clear')

            if ans == 'y':
                self.short_break_start(n+1)

        # Long break
        ans = str(input(f'{Pomodoro.text(self.pomodoros)} Do you want to start'
                        f' long break ({self.long_break:.0f} min)? (y/n) '))
        ans.strip().lower()
        os.system('clear')

        if ans == 'y':
            self.long_break_start(self.pomodoros)

        # TODO: Add exit message
        print('EXIT')


os.system('clear')
n = int(input('Pomodoros: '))
Time = float(input('(minutes) Pomodoro Time: '))
short_break = float(input('(minutes) Short break: '))
long_break = float(input('(minutes) Long break: '))
os.system('clear')

if __name__ == '__main__':
    pomodoro = Menu()
